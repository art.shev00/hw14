

window.addEventListener('load', function () {
    if (localStorage.getItem('bacColor') !== null) {
        console.log(localStorage.getItem('bacColor'));
        document.querySelector('.main-header').style.backgroundColor = '#765972';
        document.querySelectorAll('.btn-primary').forEach(el => el.style.backgroundColor = '#461022')
        document.querySelector('.section-products').style.backgroundColor = '#765972';
        document.querySelector('.section-options').style.backgroundColor = '#765972';
        let theme =   this.localStorage.getItem('theme')
    }
})


let color = document.querySelector('.btn-color')
let theme = false;

color.addEventListener('click', function () {
    if (!theme) {
        changeTheme();
        theme = true;
        localStorage.setItem('theme', 'true')
    } else {
        localStorage.clear()
        document.querySelector('.main-header').style.backgroundColor = '';
        document.querySelectorAll('.btn-primary').forEach(el => el.style.backgroundColor = '')
        document.querySelector('.section-products').style.backgroundColor = '';
        document.querySelector('.section-options').style.backgroundColor = '';
        theme = false;
        localStorage.setItem('theme', 'false')
    }
})


function changeTheme() {
    let bacColor = document.querySelector('.main-header').style.backgroundColor = '#765972';
    let btnPrimary = document.querySelectorAll('.btn-primary').forEach(el => el.style.backgroundColor = '#461022')
    let sectionProducts = document.querySelector('.section-products').style.backgroundColor = '#765972';
    let sectionOptions = document.querySelector('.section-options').style.backgroundColor = '#765972';
  

    localStorage.setItem('bacColor', '#765972')
    localStorage.setItem('btnPrimary', '#461022')
    localStorage.setItem('sectionProducts', '#765972')
    localStorage.setItem('sectionOptions', '#765972')

}
